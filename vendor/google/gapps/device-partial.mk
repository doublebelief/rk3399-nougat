#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# GApps dependencies
PRODUCT_COPY_FILES += \
	vendor/google/gapps/proprietary/system/lib64/libjni_latinimegoogle.so:system/lib64/libjni_latinimegoogle.so:google \
	vendor/google/gapps/proprietary/system/etc/sysconfig/google.xml:system/etc/sysconfig/google.xml:google \
	vendor/google/gapps/proprietary/system/etc/sysconfig/marlin_common.xml:system/etc/sysconfig/marlin_common.xml:google \
	vendor/google/gapps/proprietary/system/etc/sysconfig/google_vr_build.xml:system/etc/sysconfig/google_vr_build.xml:google \
	vendor/google/gapps/proprietary/system/etc/sysconfig/whitelist_com.android.omadm.service.xml:system/etc/sysconfig/whitelist_com.android.omadm.service.xml:google \
	vendor/google/gapps/proprietary/system/etc/sysconfig/google_build.xml:system/etc/sysconfig/google_build.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.maps.xml:system/etc/permissions/com.google.android.maps.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.widevine.software.drm.xml:system/etc/permissions/com.google.widevine.software.drm.xml:google \
	vendor/google/gapps/proprietary/system/etc/permissions/com.google.android.media.effects.xml:system/etc/permissions/com.google.android.media.effects.xml:google \
	vendor/google/gapps/proprietary/system/etc/default-permissions/default-permissions.xml:system/etc/default-permissions/default-permissions.xml:google \
	vendor/google/gapps/proprietary/system/etc/g.prop:system/etc/g.prop:google \
	vendor/google/gapps/proprietary/system/etc/preferred-apps/google.xml:system/etc/preferred-apps/google.xml:google \
	vendor/google/gapps/proprietary/system/framework/com.google.widevine.software.drm.jar:system/framework/com.google.widevine.software.drm.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.maps.jar:system/framework/com.google.android.maps.jar:google \
	vendor/google/gapps/proprietary/system/framework/com.google.android.media.effects.jar:system/framework/com.google.android.media.effects.jar:google \
	vendor/google/gapps/proprietary/optional/face/lib/libfilterpack_facedetect.so:system/lib/libfilterpack_facedetect.so:google \
	vendor/google/gapps/proprietary/optional/face/addon.d/71-gapps-faceunlock.sh:system/addon.d/71-gapps-faceunlock.sh:google \
	vendor/google/gapps/proprietary/optional/face/vendor/lib/libfrsdk.so:system/vendor/lib/libfrsdk.so:google \
	vendor/google/gapps/proprietary/optional/face/vendor/lib64/libfrsdk.so:system/vendor/lib64/libfrsdk.so:google \
	vendor/google/gapps/proprietary/optional/face/lib64/libfilterpack_facedetect.so:system/lib64/libfilterpack_facedetect.so:google \
	vendor/google/gapps/proprietary/optional/face/lib64/libfacenet.so:system/lib64/libfacenet.so:google

PRODUCT_PACKAGES += \
	GoogleOneTimeInitializer \
	GoogleExtServices \
	Phonesky \
	GoogleBackupTransport \
	GmsCoreSetupPrebuilt \
	GoogleServicesFramework \
	GoogleFeedback \
	GooglePartnerSetup \
	GoogleLoginService \
	GoogleExtShared \
	GoogleContactsSyncAdapter \
	Youtube \
	FaceLock \
	PrebuiltGmsCore \
	ConfigUpdater

